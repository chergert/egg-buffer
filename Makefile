all: test-egg-buffer

FILES = egg-buffer.c egg-buffer.h main.c

test-egg-buffer: $(FILES)
	$(CC) -o $@ -g $(WARNINGS) $(FILES) `pkg-config --libs --cflags gobject-2.0`

clean:
	rm -rf test-egg-buffer

test:
	./test-egg-buffer

WARNINGS =								\
	-Wall								\
	-Werror								\
	-Wold-style-definition						\
	-Wdeclaration-after-statement					\
	-Wredundant-decls						\
	-Wmissing-noreturn						\
	-Wshadow							\
	-Wcast-align							\
	-Wwrite-strings							\
	-Winline							\
	-Wformat-nonliteral						\
	-Wformat-security						\
	-Wswitch-enum							\
	-Wswitch-default						\
	-Winit-self							\
	-Wmissing-include-dirs						\
	-Wundef								\
	-Waggregate-return						\
	-Wmissing-format-attribute					\
	-Wnested-externs

