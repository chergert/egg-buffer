#include "egg-buffer.h"

#include <time.h>
#include <string.h>

static void
test_EggBuffer_basic (void)
{
	EggBuffer *b;

	b = egg_buffer_new();
	g_assert(b);
}

static void
test_EggBuffer_write_uint (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_uint(b, 300);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 2);
	g_assert_cmpint(buf[0], ==, 0xAC);
	g_assert_cmpint(buf[1], ==, 0x02);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_string (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_string(b, "testing");
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 8);
	g_assert_cmpint(buf[0], ==, 0x07);
	g_assert_cmpint(buf[1], ==, 't');
	g_assert_cmpint(buf[2], ==, 'e');
	g_assert_cmpint(buf[3], ==, 's');
	g_assert_cmpint(buf[4], ==, 't');
	g_assert_cmpint(buf[5], ==, 'i');
	g_assert_cmpint(buf[6], ==, 'n');
	g_assert_cmpint(buf[7], ==, 'g');
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_boolean (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_boolean(b, TRUE);
	egg_buffer_write_boolean(b, FALSE);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 2);
	g_assert_cmpint(buf[0], ==, 0x01);
	g_assert_cmpint(buf[1], ==, 0x00);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_int (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_int(b,  0);
	egg_buffer_write_int(b, -1);
	egg_buffer_write_int(b,  1);
	egg_buffer_write_int(b, -2);
	egg_buffer_write_int(b,  G_MAXINT);
	egg_buffer_write_int(b,  G_MININT);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len,     ==, 14);
	g_assert_cmpint(buf[0],  ==, 0x00);
	g_assert_cmpint(buf[1],  ==, 0x01);
	g_assert_cmpint(buf[2],  ==, 0x02);
	g_assert_cmpint(buf[3],  ==, 0x03);
	g_assert_cmpint(buf[4],  ==, 0xFE);
	g_assert_cmpint(buf[5],  ==, 0xFF);
	g_assert_cmpint(buf[6],  ==, 0xFF);
	g_assert_cmpint(buf[7],  ==, 0xFF);
	g_assert_cmpint(buf[8],  ==, 0x0F);
	g_assert_cmpint(buf[9],  ==, 0xFF);
	g_assert_cmpint(buf[10], ==, 0xFF);
	g_assert_cmpint(buf[11], ==, 0xFF);
	g_assert_cmpint(buf[12], ==, 0xFF);
	g_assert_cmpint(buf[13], ==, 0x0F);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_int64 (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_int64(b, G_MAXINT64);
	egg_buffer_write_int64(b,  0);
	egg_buffer_write_int64(b, -1);
	egg_buffer_write_int64(b,  1);
	egg_buffer_write_int64(b, -2);
	egg_buffer_write_int64(b, G_MININT64);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len,     ==, 24);
	g_assert_cmpint(buf[0],  ==, 0xFE);
	g_assert_cmpint(buf[1],  ==, 0xFF);
	g_assert_cmpint(buf[2],  ==, 0xFF);
	g_assert_cmpint(buf[3],  ==, 0xFF);
	g_assert_cmpint(buf[4],  ==, 0xFF);
	g_assert_cmpint(buf[5],  ==, 0xFF);
	g_assert_cmpint(buf[6],  ==, 0xFF);
	g_assert_cmpint(buf[7],  ==, 0xFF);
	g_assert_cmpint(buf[8],  ==, 0xFF);
	g_assert_cmpint(buf[9],  ==, 0x01);
	g_assert_cmpint(buf[10], ==, 0x00);
	g_assert_cmpint(buf[11], ==, 0x01);
	g_assert_cmpint(buf[12], ==, 0x02);
	g_assert_cmpint(buf[13], ==, 0x03);
	g_assert_cmpint(buf[14], ==, 0xFF);
	g_assert_cmpint(buf[15], ==, 0xFF);
	g_assert_cmpint(buf[16], ==, 0xFF);
	g_assert_cmpint(buf[17], ==, 0xFF);
	g_assert_cmpint(buf[18], ==, 0xFF);
	g_assert_cmpint(buf[19], ==, 0xFF);
	g_assert_cmpint(buf[20], ==, 0xFF);
	g_assert_cmpint(buf[21], ==, 0xFF);
	g_assert_cmpint(buf[22], ==, 0xFF);
	g_assert_cmpint(buf[23], ==, 0x01);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_uint64 (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_uint64(b, G_MAXUINT64);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 10);
	g_assert_cmpint(buf[0], ==, 0xFF);
	g_assert_cmpint(buf[1], ==, 0xFF);
	g_assert_cmpint(buf[2], ==, 0xFF);
	g_assert_cmpint(buf[3], ==, 0xFF);
	g_assert_cmpint(buf[4], ==, 0xFF);
	g_assert_cmpint(buf[5], ==, 0xFF);
	g_assert_cmpint(buf[6], ==, 0xFF);
	g_assert_cmpint(buf[7], ==, 0xFF);
	g_assert_cmpint(buf[8], ==, 0xFF);
	g_assert_cmpint(buf[9], ==, 0x01);
	egg_buffer_write_uint64(b, 0);
	g_assert_cmpint(buf[10], ==, 0x00);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_data (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;
	guint8 d[] = { 'a', 'b', 'c', '\0', 'd', 'e', 'f', '\0' };

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_data(b, d, sizeof(d));
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 9);
	g_assert_cmpint(buf[0], ==, 0x08);
	g_assert_cmpint(buf[1], ==, 'a');
	g_assert_cmpint(buf[2], ==, 'b');
	g_assert_cmpint(buf[3], ==, 'c');
	g_assert_cmpint(buf[4], ==, '\0');
	g_assert_cmpint(buf[5], ==, 'd');
	g_assert_cmpint(buf[6], ==, 'e');
	g_assert_cmpint(buf[7], ==, 'f');
	g_assert_cmpint(buf[8], ==, '\0');
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_double (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;
	gdouble d = 0.123123123;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_double(b, d);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 8);
	g_assert_cmpint(*((gdouble *)buf), ==, GUINT64_TO_LE(d));
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_float (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;
	gfloat f = 0.123123123;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_float(b, f);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 4);
	g_assert_cmpint(*((gfloat *)buf), ==, GUINT32_TO_LE(f));
	egg_buffer_unref(b);
}

static void
test_EggBuffer_write_tag (void)
{
	EggBuffer *b;
	const guint8 *buf = NULL;
	gsize len = 0;

	b = egg_buffer_new();
	g_assert(b);
	egg_buffer_write_tag(b, 1, EGG_BUFFER_FLOAT);
	egg_buffer_get_buffer(b, &buf, &len);
	g_assert_cmpint(len, ==, 1);
	g_assert_cmpint(buf[0], ==, 0x0D);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_uint (void)
{
	EggBuffer *b;
	guint u = 0;

	b = egg_buffer_new();
	egg_buffer_write_uint(b, 300);
	egg_buffer_write_uint(b, G_MAXUINT);
	g_assert(egg_buffer_read_uint(b, &u));
	g_assert_cmpint(u, ==, 300);
	g_assert(egg_buffer_read_uint(b, &u));
	g_assert_cmpint(u, ==, G_MAXUINT);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_uint64 (void)
{
	EggBuffer *b;
	struct timespec tv;
	guint64 u = 0;
	guint64 u2 = 0;

	b = egg_buffer_new();
	egg_buffer_write_uint64(b, G_GUINT64_CONSTANT(123));
	egg_buffer_write_uint64(b, G_MAXUINT64);
	egg_buffer_write_uint64(b, G_GUINT64_CONSTANT(123123123123));
	egg_buffer_write_uint64(b, G_GUINT64_CONSTANT(1275880764840813));
	egg_buffer_write_uint64(b, 0);
	egg_buffer_write_uint64(b, G_MAXINT);
	g_assert(egg_buffer_read_uint64(b, &u));
	g_assert_cmpuint(u, ==, G_GUINT64_CONSTANT(123));
	g_assert(egg_buffer_read_uint64(b, &u));
	g_assert_cmpuint(u, ==, G_MAXUINT64);
	g_assert(egg_buffer_read_uint64(b, &u));
	g_assert_cmpuint(u, ==, G_GUINT64_CONSTANT(123123123123));
	g_assert(egg_buffer_read_uint64(b, &u));
	g_assert_cmpuint(u, ==, G_GUINT64_CONSTANT(1275880764840813));
	g_assert(egg_buffer_read_uint64(b, &u));
	g_assert_cmpuint(u, ==, 0);
	g_assert(egg_buffer_read_uint64(b, &u));
	g_assert_cmpuint(u, ==, G_MAXINT);
	egg_buffer_unref(b);

	b = egg_buffer_new();
	clock_gettime(CLOCK_REALTIME, &tv);
	u = (tv.tv_sec * G_USEC_PER_SEC) + (tv.tv_nsec / 1000);
	egg_buffer_write_uint64(b, u);
	g_assert(egg_buffer_read_uint64(b, &u2));
	g_assert_cmpuint(u, ==, u2);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_boolean (void)
{
	EggBuffer *b;
	gboolean u = 0;

	b = egg_buffer_new();
	egg_buffer_write_boolean(b, TRUE);
	egg_buffer_write_boolean(b, FALSE);
	egg_buffer_write_boolean(b, FALSE);
	egg_buffer_write_boolean(b, TRUE);
	g_assert(egg_buffer_read_boolean(b, &u));
	g_assert_cmpint(u, ==, TRUE);
	g_assert(egg_buffer_read_boolean(b, &u));
	g_assert_cmpint(u, ==, FALSE);
	g_assert(egg_buffer_read_boolean(b, &u));
	g_assert_cmpint(u, ==, FALSE);
	g_assert(egg_buffer_read_boolean(b, &u));
	g_assert_cmpint(u, ==, TRUE);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_string (void)
{
	EggBuffer *b;
	gchar *s = NULL;

	b = egg_buffer_new();
	egg_buffer_write_string(b, "testing");
	egg_buffer_write_string(b, "abcdefghijklmnopqrstuvwxyz");
	g_assert(egg_buffer_read_string(b, &s));
	g_assert_cmpstr(s, ==, "testing");
	g_assert(egg_buffer_read_string(b, &s));
	g_assert_cmpstr(s, ==, "abcdefghijklmnopqrstuvwxyz");
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_int (void)
{
	EggBuffer *b;
	gint i = 0;
	const guint8 *d = NULL;
	gsize l = 0;

	b = egg_buffer_new();
	egg_buffer_write_int(b,  0);
	egg_buffer_write_int(b, -1);
	egg_buffer_write_int(b,  1);
	egg_buffer_write_int(b, -2);
	egg_buffer_write_int(b,  2);
	egg_buffer_write_int(b,  G_MAXINT);
	egg_buffer_write_int(b,  G_MININT);
	egg_buffer_get_buffer(b, &d, &l);
	g_assert_cmpint(l, ==, 15);
	g_assert(egg_buffer_read_int(b, &i));
	g_assert_cmpint(i, ==, 0);
	g_assert(egg_buffer_read_int(b, &i));
	g_assert_cmpint(i, ==, -1);
	g_assert(egg_buffer_read_int(b, &i));
	g_assert_cmpint(i, ==, 1);
	g_assert(egg_buffer_read_int(b, &i));
	g_assert_cmpint(i, ==, -2);
	g_assert(egg_buffer_read_int(b, &i));
	g_assert_cmpint(i, ==, 2);
	g_assert(egg_buffer_read_int(b, &i));
	g_assert_cmpint(i, ==, G_MAXINT);
	g_assert(egg_buffer_read_int(b, &i));
	g_assert_cmpint(i, ==, G_MININT);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_int64 (void)
{
	EggBuffer *b;
	gint64 i = 0;
	const guint8 *d = NULL;
	gsize l = 0;

	b = egg_buffer_new();
	egg_buffer_write_int64(b,  0);
	egg_buffer_write_int64(b, -1);
	egg_buffer_write_int64(b,  1);
	egg_buffer_write_int64(b, -2);
	egg_buffer_write_int64(b,  2);
	egg_buffer_write_int64(b,  G_MAXINT64);
	egg_buffer_write_int64(b,  G_MININT64);
	egg_buffer_get_buffer(b, &d, &l);
	g_assert_cmpint(l, ==, 25);
	g_assert(egg_buffer_read_int64(b, &i));
	g_assert_cmpint(i, ==, 0);
	g_assert(egg_buffer_read_int64(b, &i));
	g_assert_cmpint(i, ==, -1);
	g_assert(egg_buffer_read_int64(b, &i));
	g_assert_cmpint(i, ==, 1);
	g_assert(egg_buffer_read_int64(b, &i));
	g_assert_cmpint(i, ==, -2);
	g_assert(egg_buffer_read_int64(b, &i));
	g_assert_cmpint(i, ==, 2);
	g_assert(egg_buffer_read_int64(b, &i));
	g_assert_cmpint(i, ==, G_MAXINT64);
	g_assert(egg_buffer_read_int64(b, &i));
	g_assert_cmpint(i, ==, G_MININT64);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_tag (void)
{
	EggBuffer *b;
	guint f = 0;
	EggBufferTag t = 0;

	b = egg_buffer_new();
	egg_buffer_write_tag(b, 1, EGG_BUFFER_INT);
	egg_buffer_write_tag(b, 2, EGG_BUFFER_UINT);
	egg_buffer_write_tag(b, 3, EGG_BUFFER_FLOAT);
	g_assert(egg_buffer_read_tag(b, &f, &t));
	g_assert_cmpint(f, ==, 1);
	g_assert_cmpint(t, ==, EGG_BUFFER_INT);
	g_assert(egg_buffer_read_tag(b, &f, &t));
	g_assert_cmpint(f, ==, 2);
	g_assert_cmpint(t, ==, EGG_BUFFER_UINT);
	g_assert(egg_buffer_read_tag(b, &f, &t));
	g_assert_cmpint(f, ==, 3);
	g_assert_cmpint(t, ==, EGG_BUFFER_FLOAT);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_data (void)
{
	const guint8 blob[] = { 0, 0, 1, 0, 2, 0, 3, 0, 4 };
	const guint8 blob2[] = { 1, 0, 2, 3, 4 };
	EggBuffer *b;
	const guint8 *d = NULL;
	gsize l = 0;

	b = egg_buffer_new();
	egg_buffer_write_data(b, blob, sizeof(blob));
	egg_buffer_write_data(b, blob2, sizeof(blob2));
	egg_buffer_get_buffer(b, &d, &l);
	g_assert_cmpint(l, ==, 16);
	g_assert_cmpint(d[0], ==, 9);
	g_assert_cmpint(d[1], ==, blob[0]);
	g_assert_cmpint(d[2], ==, blob[1]);
	g_assert_cmpint(d[3], ==, blob[2]);
	g_assert_cmpint(d[4], ==, blob[3]);
	g_assert_cmpint(d[5], ==, blob[4]);
	g_assert_cmpint(d[6], ==, blob[5]);
	g_assert_cmpint(d[7], ==, blob[6]);
	g_assert_cmpint(d[8], ==, blob[7]);
	g_assert_cmpint(d[9], ==, blob[8]);
	g_assert_cmpint(d[10], ==, 5);
	g_assert_cmpint(d[11], ==, blob2[0]);
	g_assert_cmpint(d[12], ==, blob2[1]);
	g_assert_cmpint(d[13], ==, blob2[2]);
	g_assert_cmpint(d[14], ==, blob2[3]);
	g_assert_cmpint(d[15], ==, blob2[4]);
	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_double (void)
{
	EggBuffer *b;
	gdouble d = 0, d1 = 0;

	b = egg_buffer_new();
	egg_buffer_write_double(b,  0.123123);
	egg_buffer_write_double(b,  0.321321);
	egg_buffer_write_double(b, 24.123123);

	d1 = 0.123123;
	egg_buffer_read_double(b, &d);
	g_assert(memcmp(&d, &d1, 8) == 0);

	d1 = 0.321321;
	egg_buffer_read_double(b, &d);
	g_assert(memcmp(&d, &d1, 8) == 0);

	d1 = 24.123123;
	egg_buffer_read_double(b, &d);
	g_assert(memcmp(&d, &d1, 8) == 0);

	egg_buffer_unref(b);
}

static void
test_EggBuffer_read_float (void)
{
	EggBuffer *b;
	gfloat f = 0, f1 = 0;

	b = egg_buffer_new();
	egg_buffer_write_float(b,  0.123123);
	egg_buffer_write_float(b,  0.321321);
	egg_buffer_write_float(b, 24.123123);

	f1 = 0.123123;
	egg_buffer_read_float(b, &f);
	g_assert(memcmp(&f, &f1, 4) == 0);

	f1 = 0.321321;
	egg_buffer_read_float(b, &f);
	g_assert(memcmp(&f, &f1, 4) == 0);

	f1 = 24.123123;
	egg_buffer_read_float(b, &f);
	g_assert(memcmp(&f, &f1, 4) == 0);

	egg_buffer_unref(b);
}

gint
main (gint   argc,
      gchar *argv[])
{
	g_test_init(&argc, &argv, NULL);

	g_test_add_func("/EggBuffer/basic",         test_EggBuffer_basic);
	g_test_add_func("/EggBuffer/write_boolean", test_EggBuffer_write_boolean);
	g_test_add_func("/EggBuffer/write_data",    test_EggBuffer_write_data);
	g_test_add_func("/EggBuffer/write_double",  test_EggBuffer_write_double);
	g_test_add_func("/EggBuffer/write_float",   test_EggBuffer_write_float);
	g_test_add_func("/EggBuffer/write_int",     test_EggBuffer_write_int);
	g_test_add_func("/EggBuffer/write_int64",   test_EggBuffer_write_int64);
	g_test_add_func("/EggBuffer/write_string",  test_EggBuffer_write_string);
	g_test_add_func("/EggBuffer/write_tag",     test_EggBuffer_write_tag);
	g_test_add_func("/EggBuffer/write_uint",    test_EggBuffer_write_uint);
	g_test_add_func("/EggBuffer/write_uint64",  test_EggBuffer_write_uint64);
	g_test_add_func("/EggBuffer/read_boolean",  test_EggBuffer_read_boolean);
	g_test_add_func("/EggBuffer/read_data",     test_EggBuffer_read_data);
	g_test_add_func("/EggBuffer/read_double",   test_EggBuffer_read_double);
	g_test_add_func("/EggBuffer/read_float",    test_EggBuffer_read_float);
	g_test_add_func("/EggBuffer/read_int",      test_EggBuffer_read_int);
	g_test_add_func("/EggBuffer/read_int64",    test_EggBuffer_read_int64);
	g_test_add_func("/EggBuffer/read_string",   test_EggBuffer_read_string);
	g_test_add_func("/EggBuffer/read_tag",      test_EggBuffer_read_tag);
	g_test_add_func("/EggBuffer/read_uint",     test_EggBuffer_read_uint);
	g_test_add_func("/EggBuffer/read_uint64",   test_EggBuffer_read_uint64);

	return g_test_run();
}
